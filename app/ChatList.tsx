import React from 'react';
import ChatHeader from '../components/ChatHeader';
import ContactBox from '../components/ContactBox';
import { ContactListType } from '../ts/props/contactBox';
import avatar from '../assets/images/avatar.jpg';
import avatar2 from '../assets/images/avatar2.jpg';
import avatar3 from '../assets/images/avatar3.jpg';
import avatar4 from '../assets/images/avatar4.jpg';
import avatar5 from '../assets/images/avatar5.jpg';



const dummyData : ContactListType = [
    {
        'name' : 'Ayush Bisht',
        'avatar' : avatar
    },
    {
        'name' : 'Ronika',
        'avatar' : avatar5
    },
    {
        'name' : 'family group',
        'avatar' : avatar5
    },
    {
        'name' : 'Prashant pandey',
        'avatar' : avatar2
    },
    {
        'name' : 'Rajat ',
        'avatar' : avatar3
    }

]

const ChatList = () => {
    return (
        <div className='flex flex-col flex-nowrap bg-default grow-1 h-auto' >
            {dummyData.map(val => (
                <ContactBox contactDetails = {val} key = {val.name} />
            ))}
        </div>
    );
}

export default ChatList;
