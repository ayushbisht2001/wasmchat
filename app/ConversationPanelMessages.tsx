import React from 'react'
import Image from 'next/image'
import bgChat from '../assets/images/bgChats.png'
import css from './styles/chat_messages.module.scss'
import MsgBox from '../components/MsgBox'

export default function ConversationPanelMessages() {


    return (
        <div className={` ${css.msgBoxContainer} relative bg-gray-1000  border-l border-solid border-l-main overflow-auto`}>
            <div className= {`h-auto ${css.chatBg} `}>
            {
                [2,3,4,5].map((val) => (
                    <MsgBox  type="in" key = {val} />
                ))

                
            }
            {
                 [2,3].map((val) => (
                    <MsgBox  type="out" key = {val*10} />
                ))
            }
            {
                 [2,].map((val) => (
                    <MsgBox  type="in" key = {val*20} />
                ))
            }
            </div>

        </div>
    )
}
