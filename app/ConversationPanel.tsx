import React from 'react'
import css from './styles/conversation.module.scss'
import ConversationHeader from '../components/ConversationHeader'
import ConversationPanelMessages from './ConversationPanelMessages'
import MsgInputBox from '../components/MsgInputBox'


export default function ConversationPanel() {
  return (
    <div className= {`${css.conversationPanelContainer} h-full flex flex-col  `} >
      <div className = "basis-1/6 h-auto">
      <ConversationHeader />
      </div>
      <div className = "">
      <ConversationPanelMessages />
      </div>
      <div className = "basis-1/6 h-auto">
      <MsgInputBox />
      </div>
    </div>
  )
}
