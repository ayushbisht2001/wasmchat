import data from '../../assets/data/emojis.json';
import { EmojiData } from '../../ts/emojis';


const emojis : Record<string, EmojiData[] > = data;


export default emojis ;
