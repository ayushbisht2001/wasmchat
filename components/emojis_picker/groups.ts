const groups = [
    'smileys_people',
    'animals_nature',
    'food_drink',
    'activities',
    'travel_places',
    'objects',
    'symbols',
    'flags'
  ];
  
  export const mapGroupsWithLabels :  Record<string, string>  = {
    smileys_people :  'Smileys & People',
    animals_nature : 'Animals & Nature',
    food_drink : 'Food & Drink',
    travel_places : 'Travel & Places',
    activities : 'Activities',
    objects : 'Objects',
    symbols : 'Symbols',
    flags : 'Flags'
  }


  export default groups;
  